PixelsDaily
=======================
Name:	Subtle PSD Dividers
Type:	PSD
Author:	Mee Rebirth


Notes
=======================
A set of incredibly useful PSD dividers. These are perfect for use in mockups, detailed interfaces, or in your next web design for seperating content blocks in an interesting way. Whether you want something with just a tiny hint of visual flair, or a more definitive divider, there's a style here to suit.




Free Resource Licensing
=======================
All our free resources are licensed under a Creative Commons Attribution license. This license lets you distribute, remix, tweak, and build upon your work, even commercially, as long as you credit PixelsDaily for the original creation:

http://creativecommons.org/licenses/by/3.0/