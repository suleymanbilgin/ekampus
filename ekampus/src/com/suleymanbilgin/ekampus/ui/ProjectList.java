package com.suleymanbilgin.ekampus.ui;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.custom.CustomFragment;
import com.suleymanbilgin.ekampus.model.Data;

public class ProjectList extends CustomFragment
{
	/** The Activity list. */
	private ArrayList<Data> projectList;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.project, null);

		loadProjectList();
		ListView list = (ListView) v.findViewById(R.id.list);
		list.setAdapter(new NoteAdapter());
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3)
			{

			}
		});

		setTouchNClick(v.findViewById(R.id.tab1));
		setTouchNClick(v.findViewById(R.id.tab2));
		return v;
	}

	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.tab1)
		{
			getView().findViewById(R.id.tab2).setEnabled(true);
			v.setEnabled(false);
		}
		else if (v.getId() == R.id.tab2)
		{
			getView().findViewById(R.id.tab1).setEnabled(true);
			v.setEnabled(false);
		}
	}

	private void loadProjectList()
	{
		ArrayList<Data> pList = new ArrayList<Data>();
		pList.add(new Data("Yeni Android Uygulamas�", "�zel Projem", 1));
		pList.add(new Data("Ad�mlar", "iOS yaz�l�m geli�tirme", 2));
		pList.add(new Data("Acele et!", "Acil!", 1));
		

		projectList = new ArrayList<Data>(pList);
		projectList.addAll(pList);
		projectList.addAll(pList);
	}

	private class NoteAdapter extends BaseAdapter
	{

		@Override
		public int getCount()
		{
			return projectList.size();
		}

		@Override
		public Data getItem(int arg0)
		{
			return projectList.get(arg0);
		}

		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.project_item, null);

			Data c = getItem(pos);
			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(c.getTitle1());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getDesc());

			if (c.getImage1() == 0)
				v.findViewById(R.id.vCount).setVisibility(View.INVISIBLE);
			else
			{
				v.findViewById(R.id.vCount).setVisibility(View.VISIBLE);

				lbl = (TextView) v.findViewById(R.id.l1);
				lbl.setText((pos + 1) * 2 + "");
				lbl.setEnabled(c.getImage1() != 1);

				lbl = (TextView) v.findViewById(R.id.l2);
				lbl.setText((pos + 1) * 4 + "");
				lbl.setEnabled(c.getImage1() != 2);

				lbl = (TextView) v.findViewById(R.id.l3);
				lbl.setText((pos + 1) * 3 + "");
				lbl.setEnabled(c.getImage1() != 3);
			}
			return v;
		}

	}
}
