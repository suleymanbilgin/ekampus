package com.suleymanbilgin.ekampus.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.NewChat;
import com.suleymanbilgin.ekampus.custom.CustomFragment;
import com.suleymanbilgin.ekampus.model.ChatItem;

/**
 * The Class NoteList is the Fragment class that is launched when the user
 * clicks on Notes option in Left navigation drawer. It simply display a dummy
 * list of notes. You need to write actual implementation for loading and
 * displaying notes
 */
public class NotificationList extends CustomFragment {

	/** The Note list. */
	private ArrayList<ChatItem> noteList;
	private RequestQueue mRequestQueue;
	private ArrayList<ChatItem> nList = new ArrayList<ChatItem>();
	private NoteAdapter noteAdapter;
	
	/** SharedPreferences variable */
	protected SharedPreferences share;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
	 * android.view.ViewGroup, android.os.Bundle)
	 */
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.note, null);
		mRequestQueue = Volley.newRequestQueue(getActivity());
		// Get Shared Preferences
		share = this.getActivity().getSharedPreferences("appdata", 0);
		noteList = new ArrayList<ChatItem>(nList);
		loadNoteList();
		
		ListView list = (ListView) v.findViewById(R.id.list);
		noteAdapter = new NoteAdapter();
		list.setAdapter(noteAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {

			}
		});

		setTouchNClick(v.findViewById(R.id.tab1));
		setTouchNClick(v.findViewById(R.id.tab2));
		setTouchNClick(v.findViewById(R.id.btnNewChat));
		return v;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v.getId() == R.id.tab1) {
			getView().findViewById(R.id.tab2).setEnabled(true);
			v.setEnabled(false);
		} else if (v.getId() == R.id.tab2) {
			getView().findViewById(R.id.tab1).setEnabled(true);
			v.setEnabled(false);
		} else if (v.getId() == R.id.btnNewChat)
			startActivity(new Intent(getActivity(), NewChat.class));
	}

	/**
	 * This method currently loads a dummy list of Notes. You can write the
	 * actual implementation of loading Notes.
	 */
	private void loadNoteList() {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();
		
		String URL = getString(R.string.service_url)
				+ getString(R.string.duyurular);
		
		// Sending URL to JsonArrayRequest
		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {

						try {

							JSONObject tmp;

							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								//Variables
								String Baslik = tmp.getString("Baslik");
								String Icerik = tmp.getString("Icerik");
								String DersAdi = tmp.getString("DersAdi");
								String Ad = tmp.getString("Ad");
								
								// Fill the list
								noteList.add(new ChatItem(Baslik, DersAdi,
										Icerik, Ad, 01, true, false));
							}
							noteList.addAll(nList);
							noteAdapter.notifyDataSetChanged();
						} catch (JSONException e) {
							e.printStackTrace();
						}
						pd.dismiss();
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

						pd.dismiss();
					}
				});
		mRequestQueue.add(json);
	}

	/**
	 * The Class CutsomAdapter is the adapter class for Note ListView. The
	 * currently implementation of this adapter simply display static dummy
	 * contents. You need to write the code for displaying actual contents.
	 */
	private class NoteAdapter extends BaseAdapter
	{

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return noteList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public ChatItem getItem(int arg0)
		{
			return noteList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0)
		{
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2)
		{
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.note_item, null);

			ChatItem c = getItem(pos);
			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(c.getName());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getDate());

			lbl = (TextView) v.findViewById(R.id.lbl3);
			lbl.setText(c.getTitle());

			lbl = (TextView) v.findViewById(R.id.lbl4);
			lbl.setText(c.getMsg());

			ImageView img = (ImageView) v.findViewById(R.id.img2);
			img.setImageResource(c.isGroup() ? R.drawable.ic_group
					: R.drawable.ic_lock);

			return v;
		}

	}
}
