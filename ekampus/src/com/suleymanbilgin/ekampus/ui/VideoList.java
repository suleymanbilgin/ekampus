package com.suleymanbilgin.ekampus.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.VideoListActivity;
import com.suleymanbilgin.ekampus.custom.CustomFragment;
import com.suleymanbilgin.ekampus.model.Lesson;

public class VideoList extends CustomFragment {
	/** The Activity list. */
	private ArrayList<Lesson> lessonList;
	private ArrayList<Lesson> lList = new ArrayList<Lesson>();
	private LessonAdapter lessonAdapter;

	SharedPreferences share;
	RequestQueue mRequestQueue;

	String userID;
	String userPassword;

	Lesson lessonModel;
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.video_list, null);
		mRequestQueue = Volley.newRequestQueue(getActivity());
		// Get Shared Preferences
		share = this.getActivity().getApplicationContext()
				.getSharedPreferences("appdata", 0);
		userID = String.valueOf(share.getString("kullaniciAdi", ""));
		userPassword = String.valueOf(share.getString("sifreduz", ""));
		Log.e("***", String.valueOf(share.getString("sifreduz", "")));
		ListView list = (ListView) v.findViewById(R.id.list);
		lessonList = new ArrayList<Lesson>(lList);

		loadLessonList();
		lessonAdapter = new LessonAdapter();
		list.setAdapter(lessonAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				lessonModel = (Lesson) arg0.getAdapter().getItem(pos);
				Toast.makeText(getActivity(), lessonModel.getLessonID(), Toast.LENGTH_LONG).show();
				Intent videoListActivity = new Intent(getActivity(), VideoListActivity.class);
				videoListActivity.putExtra("lessonID", 	lessonModel.getLessonID());
				startActivity(videoListActivity);
			}
		});
		return v;
	}
	
	private void loadLessonList() {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();

		String URL = getString(R.string.service_url)
				+ getString(R.string.ders_listesi) + "?kullaniciAdi=" + userID
				+ "&sifre=" + userPassword;

		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {
					public void onResponse(JSONArray response) {
						try {
							Log.e("****", String.valueOf(response));
							JSONObject tmp;

							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								String name = tmp.getString("DersAdi");
								String code = tmp.getString("DersKodu");
								String teacher = tmp.getString("DersOgretmen");
								String lessonID = tmp.getString("DersID");

								// Fill the list
								lList.add(new Lesson(name, code, teacher,
										lessonID));
							}

							lessonList.addAll(lList);
							lessonAdapter.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					};
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("****", String.valueOf(error));
						pd.dismiss();
					}
				});
		mRequestQueue.add(json);
	}

	private class LessonAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return lessonList.size();
		}

		@Override
		public Lesson getItem(int arg0) {
			return lessonList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.lesson_item, null);

			Lesson c = getItem(pos);
			TextView tvLessonName = (TextView) v
					.findViewById(R.id.tv_lesson_name);
			tvLessonName.setText(c.getName());

			TextView tvLessonCode = (TextView) v
					.findViewById(R.id.tv_lesson_code);
			tvLessonCode.setText(c.getCode());

			TextView tvLessonTeacher = (TextView) v
					.findViewById(R.id.tv_lesson_teacher);
			tvLessonTeacher.setText(c.getTeacher());

			return v;
		}

	}
}
