package com.suleymanbilgin.ekampus.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.ChatActivity;
import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.custom.CustomFragment;
import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.helper.Utils;
import com.suleymanbilgin.ekampus.model.ChatItem;

/**
 * The Class ChatList is the Fragment class that is launched when the user
 * clicks on Chats option in Left navigation drawer. It shows a dummy list of
 * user's chats. You need to write your own code to load and display actual
 * chat.
 */
public class ChatList extends CustomFragment {

	/** The Chat list. */
	private ArrayList<ChatItem> chatList;
	private ArrayList<ChatItem> clist = new ArrayList<ChatItem>();
	private ChatAdapter chatAdapter;
	private SharedPreferences share;
	private RequestQueue mRequestQueue;
	ChatItem chatModel;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.chat_list, null);

		mRequestQueue = Volley.newRequestQueue(getActivity());
		share = this.getActivity().getApplicationContext()
				.getSharedPreferences("appdata", 0);
		chatList = new ArrayList<ChatItem>(clist);
		loadChatList();
		chatAdapter = new ChatAdapter();
		ListView list = (ListView) v.findViewById(R.id.list);
		list.setAdapter(chatAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				// getFragmentManager().beginTransaction()
				// .replace(R.id.content_frame, new Chat())
				// .addToBackStack("Chat").commit();
				chatModel = (ChatItem) arg0.getAdapter().getItem(pos);
				Utils.showToast(String.valueOf(chatModel.getID()),
						getActivity());
				Intent intent = new Intent(getActivity(), ChatActivity.class);
				intent.putExtra("friend_id", chatModel.getID());
				AppLog.Log("friend_id:: ", String.valueOf(chatModel.getID()));
				startActivity(intent);
			}
		});

		// setTouchNClick(v.findViewById(R.id.btnNewChat));
		return v;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		// if (v.getId() == R.id.btnNewChat)
		// startActivity(new Intent(getActivity(), NewChat.class));
	}

	/**
	 * This method currently loads a dummy list of chats. You can write the
	 * actual implementation of loading chats.
	 */
	private void loadChatList() {
		final ProgressDialog pd = new ProgressDialog(getActivity());
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();

		String URL = getString(R.string.service_url)
				+ getString(R.string.mesaj_gonderenler_listesi) + "?user_id="
				+ share.getString("kullaniciID", "0");

		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {
					public void onResponse(JSONArray response) {
						try {
							Log.e("****", String.valueOf(response));
							JSONObject tmp;

							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								int id = tmp.getInt("ID");
								String name = tmp.getString("Name");

								// Fill the list
								clist.add(new ChatItem(name, id));
							}

							chatList.addAll(clist);
							chatAdapter.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					};
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e("****", String.valueOf(error));
						pd.dismiss();
					};
				});
		mRequestQueue.add(json);
	}

	/**
	 * The Class ChatAdapter is the adapter class for Chat ListView. The
	 * currently implementation of this adapter simply display static dummy
	 * contents. You need to write the code for displaying actual contents.
	 */
	private class ChatAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return chatList.size();
		}

		@Override
		public ChatItem getItem(int arg0) {
			return chatList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressWarnings("unused")
		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item, null);

			ChatItem c = getItem(pos);
			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(c.getName());

			int id = c.getID();

			ImageView img = (ImageView) v.findViewById(R.id.img1);

			return v;
		}

	}
}
