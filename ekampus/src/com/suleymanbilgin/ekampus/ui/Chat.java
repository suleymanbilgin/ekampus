package com.suleymanbilgin.ekampus.ui;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.custom.CustomFragment;
import com.suleymanbilgin.ekampus.model.Conversation;

public class Chat extends CustomFragment {

	/** The Conversation list. */
	private ArrayList<Conversation> convList;

	/** The chat adapter. */
	private ChatAdapter adp;

	/** The Editext to compose the message. */
	private EditText txt;

	SharedPreferences share;
	
	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.chat, null);
		
		share = getActivity().getSharedPreferences("appdata", 0);
		
		loadConversationList();
		ListView list = (ListView) v.findViewById(R.id.list);
		adp = new ChatAdapter();
		list.setAdapter(adp);
		list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		list.setStackFromBottom(true);

		txt = (EditText) v.findViewById(R.id.txt);
		txt.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_FLAG_MULTI_LINE);

		setTouchNClick(v.findViewById(R.id.btnSend));
		return v;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v.getId() == R.id.btnSend) {
			sendMessage();
		}

	}

	private void sendMessage() {
		if (txt.length() == 0)
			return;

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txt.getWindowToken(), 0);

		String s = txt.getText().toString();
		convList.add(new Conversation(s, "12:00 AM", true, true));
		convList.add(new Conversation("Hello, this is auto reply for you...",
				"12:00 AM", false, true));
		adp.notifyDataSetChanged();
		txt.setText(null);
	}

	private void loadConversationList()
	{
		convList = new ArrayList<Conversation>();
		convList.add(new Conversation("Merhaba...", "12:45 AM", true, true));
		convList.add(new Conversation("Merhaba, Nas�l yard�mc� olabilirim?", "12:47 AM",
				false, true));
		convList.add(new Conversation("Chat arac�n� be�endiniz mi?",
				"12:49 AM", true, true));
		convList.add(new Conversation("�ok g�zel!", "12:50 AM",
				false, true));
		
//		String url = getString(R.string.service_url) + getString(R.string.mesaj_listesi) + 
//				"?userid=" + share.getString("kullaniciID", "") + "&friendid="
//				+ "4";
		
		

	}

	private class ChatAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return convList.size();
		}

		@Override
		public Conversation getItem(int arg0) {
			return convList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			Conversation c = getItem(pos);
			if (c.isSent())
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item_sent, null);
			else
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item_rcv, null);

			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(c.getDate());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getMsg());

			lbl = (TextView) v.findViewById(R.id.lbl3);
			if (c.isSuccess())
				lbl.setText("Delivered");
			else
				lbl.setText("");

			return v;
		}

	}
}
