package com.suleymanbilgin.ekampus;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.model.Announcement;

public class AnnouncementDetailActivity extends CustomActivity {

	RequestQueue mRequestQueue;

	/** The Activity list. */
	private ArrayList<Announcement> announcementList;
	private ArrayList<Announcement> aList = new ArrayList<Announcement>();
	private AnnouncementAdapter announcementAdapter;

	private Announcement tempAnnouncement;

	SharedPreferences share;

	String userID;
	String userPassword;
	String value = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announcement_activity);
		share = getApplicationContext().getSharedPreferences("appdata", 0);
		init();

		Button btn_new_message = (Button) findViewById(R.id.btnNewChat);
		if (share.getString("userType", "0").equals("1")) {
			btn_new_message.setVisibility(View.INVISIBLE);
		} else if (share.getString("userType", "0").equals("2")) {
			btn_new_message.setVisibility(View.VISIBLE);
		}
		btn_new_message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AnnouncementDetailActivity.this,
						AddAnnouncementActivity.class);
				intent.putExtra("lessonID", value);
				startActivity(intent);
			}
		});
	}

	private void init() {
		mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		announcementList = new ArrayList<Announcement>(aList);
		ListView list = (ListView) findViewById(R.id.list);

		value = "";
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("lessonID");
		}

		loadAnnouncementList(value);

		announcementAdapter = new AnnouncementAdapter();
		list.setAdapter(announcementAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent intent = new Intent(getApplicationContext(),
						AnnouncementActivity.class);
				tempAnnouncement = (Announcement) arg0.getAdapter().getItem(
						arg2);
				intent.putExtra("title", tempAnnouncement.getTitle());
				intent.putExtra("content", tempAnnouncement.getContent());
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	private void loadAnnouncementList(String lessonID) {
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();

		String URL = getString(R.string.service_url)
				+ getString(R.string.duyuru_listesi) + "?dersID=" + lessonID;

		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray response) {
						Log.e("****", String.valueOf(response));
						JSONObject tmp;
						try {
							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								@SuppressWarnings("unused")
								String DersAdi = tmp.getString("DersAdi");
								String Hafta = tmp.getString("DersBolumAdi");
								String Baslik = tmp.getString("Baslik");
								String icerik = tmp.getString("icerik");

								// Fill the list
								aList.add(new Announcement(icerik, "1", Baslik,
										Hafta));
							}

							announcementList.addAll(aList);
							announcementAdapter.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {

					}
				});
		mRequestQueue.add(json);
	}

	private class AnnouncementAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return announcementList.size();
		}

		@Override
		public Announcement getItem(int arg0) {
			return announcementList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getApplicationContext()).inflate(
						R.layout.announcement_item, null);

			Announcement c = getItem(pos);
			TextView tvAnnouncementTitle = (TextView) v
					.findViewById(R.id.tv_announcement_title);
			tvAnnouncementTitle.setText(c.getTitle());

			TextView tvAnnouncementCode = (TextView) v
					.findViewById(R.id.tv_announcement_content);
			tvAnnouncementCode.setText(c.getContent());

			TextView tvAnnouncementWeek = (TextView) v
					.findViewById(R.id.tv_announcement_week);
			tvAnnouncementWeek.setText(c.getWeek());

			return v;
		}

	}
}
