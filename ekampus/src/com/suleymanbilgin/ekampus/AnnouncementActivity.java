package com.suleymanbilgin.ekampus;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.suleymanbilgin.ekampus.custom.CustomActivity;

public class AnnouncementActivity extends CustomActivity {

	private TextView tv_announcement_title;
	private TextView tv_announcement_content;
	Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announcement_detail);
		init();
		if (extras == null) {

		} else {
			tv_announcement_title.setText(extras.getString("title"));
			tv_announcement_content.setText(extras.getString("content"));
		}
	}

	public void init() {
		tv_announcement_title = (TextView) findViewById(R.id.tv_title_announcement);
		tv_announcement_content = (TextView) findViewById(R.id.tv_content_announcement);
		extras = getIntent().getExtras();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}
}
