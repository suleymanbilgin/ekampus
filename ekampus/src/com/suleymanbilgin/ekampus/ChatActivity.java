package com.suleymanbilgin.ekampus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.model.Conversation;

public class ChatActivity extends CustomActivity {
	/** The Conversation list. */
	private ArrayList<Conversation> convList;
	private ArrayList<Conversation> cList = new ArrayList<Conversation>();

	/** The chat adapter. */
	private ChatAdapter adp;

	/** The Editext to compose the message. */
	private EditText txt;

	SharedPreferences share;

	private RequestQueue mRequestQueue;
	int value = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);

		mRequestQueue = Volley.newRequestQueue(getApplicationContext());

		share = this.getSharedPreferences("appdata", 0);
		convList = new ArrayList<Conversation>(cList);

		ListView list = (ListView) findViewById(R.id.list);

		value = 0;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getInt("friend_id");
		}

		loadConversationList(value);

		adp = new ChatAdapter();
		list.setAdapter(adp);
		list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		list.setStackFromBottom(true);

		txt = (EditText) findViewById(R.id.txt);
		txt.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_FLAG_MULTI_LINE);

		setTouchNClick(R.id.btnSend);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v.getId() == R.id.btnSend) {
			sendMessage(value);
		}

	}

	private void sendMessage(int friend_id) {
		if (txt.length() == 0)
			return;
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(false);
		pd.show();
		InputMethodManager imm = (InputMethodManager) this
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txt.getWindowToken(), 0);

		final String s = txt.getText().toString();
//		convList.add(new Conversation(s, "12:00 AM", true, true));
//		convList.add(new Conversation("Hello, this is auto reply for you...",
//				"12:00 AM", false, true));

		String URL = getString(R.string.service_url)
				+ getString(R.string.mesaj_gonder) + "?uid="
				+ share.getString("kullaniciID", "") + "&fid="
				+ String.valueOf(friend_id) + "&message=" + s;
		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {
					@SuppressLint("SimpleDateFormat") @Override
					public void onResponse(JSONArray response) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
						String currentDateandTime = sdf.format(new Date());
						convList.add(new Conversation(s, currentDateandTime, true, true));
						adp.notifyDataSetChanged();
						pd.dismiss();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						pd.dismiss();
					}
				});
		mRequestQueue.add(json);

		txt.setText(null);
	}

	private void loadConversationList(int friend_id) {
		// convList = new ArrayList<Conversation>();
		// convList.add(new Conversation("Merhaba...", "12:45 AM", true, true));
		// convList.add(new Conversation("Merhaba, Nas�l yard�mc� olabilirim?",
		// "12:47 AM", false, true));
		// convList.add(new Conversation("Chat arac�n� be�endiniz mi?",
		// "12:49 AM", true, true));
		// convList.add(new Conversation("�ok g�zel!", "12:50 AM", false,
		// true));
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(false);
		pd.show();
		String url = getString(R.string.service_url)
				+ getString(R.string.mesaj_listesi) + "?userid="
				+ share.getString("kullaniciID", "") + "&friendid="
				+ String.valueOf(friend_id);

		AppLog.Log("URL:: ", url);

		JsonArrayRequest json = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					public void onResponse(JSONArray response) {
						try {
							Log.e("****", String.valueOf(response));
							JSONObject tmp;

							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								String message = tmp.getString("message");
								String date = tmp.getString("date");
								String user_id = tmp.getString("userid");
								if (share.getString("kullaniciID", "0").equals(
										user_id)) {
									// Fill the list
									cList.add(new Conversation(message, date,
											true, true));
								} else {
									cList.add(new Conversation(message, date,
											false, true));
								}

							}

							convList.addAll(cList);
							adp.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					};
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

					};
				});
		mRequestQueue.add(json);
	}

	private class ChatAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return convList.size();
		}

		@Override
		public Conversation getItem(int arg0) {
			return convList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			Conversation c = getItem(pos);
			if (c.isSent())
				v = LayoutInflater.from(getApplicationContext()).inflate(
						R.layout.chat_item_sent, null);
			else
				v = LayoutInflater.from(getApplicationContext()).inflate(
						R.layout.chat_item_rcv, null);

			TextView lbl = (TextView) v.findViewById(R.id.lbl1);
			lbl.setText(c.getDate());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getMsg());

			return v;
		}

	}
}
