package com.suleymanbilgin.ekampus;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.model.VideoItem;

public class VideoListActivity extends CustomActivity implements
		OnItemClickListener {
	private static final String TAG = "LessonVideosActivity";

	RequestQueue mRequestQueue;

	/** The Activity list. */
	private ArrayList<VideoItem> lessonVideoList;
	private ArrayList<VideoItem> lfList = new ArrayList<VideoItem>();
	private LessonVideoAdapter lessonFileAdapter;

	SharedPreferences share;

	String userID;
	String userPassword;

	private String value = "";
	@SuppressWarnings("unused")
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lesson_videos_activity);

		mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		lessonVideoList = new ArrayList<VideoItem>(lfList);
		ListView list = (ListView) findViewById(R.id.list);

		mProgressDialog = new ProgressDialog(VideoListActivity.this);

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			value = extras.getString("lessonID");
		}

		loadLessonFileList(value);

		lessonFileAdapter = new LessonVideoAdapter();
		list.setAdapter(lessonFileAdapter);
		list.setOnItemClickListener(this);
	}

	private void loadLessonFileList(String lessonID) {
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();

		String URL = getString(R.string.service_url)
				+ getString(R.string.video_listesi) + "?dersID=" + lessonID;

		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						AppLog.Log(TAG, response.toString());
						JSONObject tmp;
						try {
							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								String url = tmp.getString("videourl");
								String name = tmp.getString("Ad");
								String dosyaHafta = tmp.getString("hafta");
								String silinme = tmp.getString("silinme");

								if (silinme.equals("False")) {
									// Fill the list
									lfList.add(new VideoItem(name, url,
											dosyaHafta));
								} else {

								}
							}

							lessonVideoList.addAll(lfList);
							lessonFileAdapter.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						pd.dismiss();
					}
				});

		mRequestQueue.add(json);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	private class LessonVideoAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return lessonVideoList.size();
		}

		@Override
		public VideoItem getItem(int arg0) {
			return lessonVideoList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getApplicationContext()).inflate(
						R.layout.lesson_file_item, null);

			VideoItem c = getItem(pos);
			TextView tvAnnouncementTitle = (TextView) v
					.findViewById(R.id.tv_lesson_file_title);
			tvAnnouncementTitle.setText(c.getName());

			TextView tvAnnouncementCode = (TextView) v
					.findViewById(R.id.tv_lesson_file_content);
			tvAnnouncementCode.setText("");

			TextView tvAnnouncementWeek = (TextView) v
					.findViewById(R.id.tv_lesson_file_week);
			tvAnnouncementWeek.setText(c.getWeek());

			return v;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View View, int position,
			long id) {
		AppLog.Log(TAG, String.valueOf(position));

		VideoItem tempVideoFile = (VideoItem) parent
				.getItemAtPosition(position);

		// AppLog.Log(TAG, "Temp File Informations :: " +
		// tempLessonFile.getUrl()
		// + " " + tempLessonFile.getFileType());

		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(tempVideoFile
				.getUrl())));
	}
}
