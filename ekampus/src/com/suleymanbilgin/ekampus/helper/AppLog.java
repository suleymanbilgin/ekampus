package com.suleymanbilgin.ekampus.helper;

import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.MyApplication;

public class AppLog {
	public static final boolean IS_DEGUG = true;

	public static final void Log(String tag, String message) {
		if (IS_DEGUG)
			android.util.Log.i(tag, message);
	}

	public static final void Log(String message) {
		if (IS_DEGUG)
			android.util.Log.i(
					MyApplication.getContext().getString(R.string.app_name),
					message);
	}
}
