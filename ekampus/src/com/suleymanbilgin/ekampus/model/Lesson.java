package com.suleymanbilgin.ekampus.model;

public class Lesson {

	private String name;
	private String code;
	private String teacher;
	private String lessonID;
	
	public Lesson(String name, String code, String teacher, String lessonID) {
		this.name = name;
		this.code = code;
		this.teacher = teacher;
		this.lessonID = lessonID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	
	public String getLessonID() {
		return lessonID;
	}

	public void setLessonID(String lessonID) {
		this.lessonID = lessonID;
	} 
}
