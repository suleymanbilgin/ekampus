package com.suleymanbilgin.ekampus.model;

public class Announcement {
	private String content;
	private String announcementID;
	private String Title;
	private String week;

	public Announcement(String content, String announcementID, String Title,
			String week) {
		this.content = content;
		this.announcementID = announcementID;
		this.Title = Title;
		this.week = week;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getAnnouncementID() {
		return announcementID;
	}

	public void setAnnouncementID(String announcementID) {
		this.announcementID = announcementID;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String Title) {
		this.Title = Title;
	}
}
