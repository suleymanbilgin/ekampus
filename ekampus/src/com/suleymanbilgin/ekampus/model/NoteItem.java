package com.suleymanbilgin.ekampus.model;

/**
 * The Class NoteItem is a Java Bean class that can be used for representing a
 * Note item.
 */
public class NoteItem {

	/** The title. */
	private String title;

	/** The content. */
	private String content;

	/** The date. */
	private String date;

	/** The is group. */
	private boolean isActive;

	/**
	 * Instantiates a new note item.
	 * 
	 * @param title
	 *            the title
	 * @param msg
	 *            the content
	 * @param date
	 *            the date
	 * @param isGroup
	 *            the is active
	 */
	public NoteItem(String title, String content, String date, boolean isActive) {
		this.date = date;
		this.isActive = isActive;
		this.content = content;
		this.title = title;
	}
	
	/**
	 * Instantiates a new note item.
	 * 
	 * @param title
	 *            the title
	 * @param msg
	 *            the content
	 * @param isActive
	 *            the is active
	 */
	public NoteItem(String title, String content, boolean isActive) {
		this.content = content;
		this.title = title;
		this.isActive = isActive;
	}
	
	/**
	 * Instantiates a new note item.
	 * 
	 * @param title
	 *            the title
	 * @param msg
	 *            the content
	 * @param isActive
	 *            the is active
	 */
	public NoteItem(String title, String content, String date) {
		this.content = content;
		this.title = title;
		this.date = date;
	}
	
	/**
	 * Instantiates a new note item.
	 * 
	 * @param title
	 *            the title
	 * @param msg
	 *            the content
	 */
	public NoteItem(String title, String content) {
		this.content = content;
		this.title = title;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String Content) {
		this.content = Content;
	}

	/**
	 * Checks if is online.
	 * 
	 * @return true, if is online
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * Sets the online.
	 * 
	 * @param online
	 *            the new online
	 */
	public void setisActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the date.
	 * 
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}
}
