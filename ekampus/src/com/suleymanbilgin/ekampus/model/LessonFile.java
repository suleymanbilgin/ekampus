package com.suleymanbilgin.ekampus.model;

public class LessonFile {
	private String url;
	private String fileID;
	private String Name;
	private String week;
	private String fileType;

	public LessonFile(String fileID, String url, String Name, String fileType,
			String week) {
		this.fileID = fileID;
		this.url = url;
		this.Name = Name;
		this.week = week;
		this.fileType = fileType;
	}

	public String getFileID() {
		return fileID;
	}

	public void setFileID(String fileID) {
		this.fileID = fileID;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
}
