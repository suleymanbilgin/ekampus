package com.suleymanbilgin.ekampus.model;

public class VideoItem {
	private String name;
	private String week;
	private String url;
	
	public VideoItem(String name, String url, String week) {
		this.name = name;
		this.url = url;
		this.week = week;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

}
