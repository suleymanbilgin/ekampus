package com.suleymanbilgin.ekampus.internet;

import java.io.File;

import com.suleymanbilgin.ekampus.helper.Constant;

import android.os.Environment;

public class Urls {
	public static final String BASE_URL = "http://www.e-kampus.net/";

	public static final String BASE_URL_JSON = "http://www.e-kampus.net/json/";
	
	public static final String BASE_IMAGE = BASE_URL + "images/";

	public static final String BASE_FILES = BASE_URL + "dosyalar/dersler/";
	
	public static final String BASE_STICKER = BASE_IMAGE + "sticker/";

	public static final String BASE_LOCAL_IMAGE_PATH = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ File.separator
			+ Constant.ROOT_FOLDER_NAME
			+ File.separator
			+ Constant.FOLDER_IMAGE;
	
	public static final String BASE_LOCAL_PDF_PATH = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ File.separator
			+ Constant.ROOT_FOLDER_NAME
			+ File.separator
			+ Constant.FOLDER_PDF;

}
