package com.suleymanbilgin.ekampus;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class MyApplication extends Application {

	private static MyApplication instance;
	private static Context appContext;
	@SuppressWarnings("unused")
	private static String version = "";

	@Override
	public void onCreate() {
		super.onCreate();
		appContext = getApplicationContext();

		PackageInfo info;
		try {
			PackageManager manager = MyApplication.getContext()
					.getPackageManager();
			info = manager.getPackageInfo(MyApplication.getContext()
					.getPackageName(), 0);
			version = info.versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		instance = this;

	}

	public static void initInstance() {
		if (instance == null)
			instance = new MyApplication(); // create the instance
	}

	public static Context getContext() {
		return appContext;
	}

	public static MyApplication getInstance() {
		return instance;
	}

}
