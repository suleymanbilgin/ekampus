package com.suleymanbilgin.ekampus;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.suleymanbilgin.ekampus.R;
import com.google.android.gcm.GCMRegistrar;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.gcm.ServerUtilities;
import com.suleymanbilgin.ekampus.gcm.WakeLocker;
import com.suleymanbilgin.ekampus.model.Data;
import com.suleymanbilgin.ekampus.ui.AboutChat;
import com.suleymanbilgin.ekampus.ui.AnnouncementList;
import com.suleymanbilgin.ekampus.ui.ChatList;
import com.suleymanbilgin.ekampus.ui.LeftNavAdapter;
import com.suleymanbilgin.ekampus.ui.LessonList;
import com.suleymanbilgin.ekampus.ui.VideoList;

/**
 * The Class MainActivity is the base activity class of the application. This
 * activity is launched after the Splash and it holds all the Fragments used in
 * the app. It also creates the Navigation Drawer on left side.
 */
public class MainActivity extends CustomActivity {
	/* GCM Datas */
	static final String SENDER_ID = "309424351562";
	static final String DISPLAY_MESSAGE_ACTION = "com.suleymanbilgin.ekampus.permission.C2D_MESSAGE";
	static final String EXTRA_MESSAGE = "message";
	AsyncTask<Void, Void, Void> mRegisterTask;
	public String myDeviceId = null;

	String kullaniciID2;

	/** The drawer layout. */
	private DrawerLayout drawerLayout;

	/** ListView for left side drawer. */
	private ListView drawerLeft;

	/** The drawer toggle. */
	private ActionBarDrawerToggle drawerToggle;

	SharedPreferences share;
	Bundle extras;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		try {
			extras = getIntent().getExtras();
			kullaniciID2 = extras.getString("kullaniciID2");
		} catch (Exception e) {

		}

		share = getApplicationContext().getSharedPreferences("appdata", 0);
		myDeviceId = getRequesterID();

		// GCM control for devices.
		GCMRegistrar.checkDevice(this);
		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(this);

		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				DISPLAY_MESSAGE_ACTION));
		
		// Get GCM registration id
		String regIdTmp = GCMRegistrar.getRegistrationId(this);

		final String regId = regIdTmp;

		GCMRegistrar.setRegisteredOnServer(getApplicationContext(), false);
		
		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			// Device is already registered on GCM
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				Toast.makeText(getApplicationContext(),
						"Already registered with GCM", Toast.LENGTH_LONG)
						.show();
				Log.e("Register Status", "Already registered with GCM");
			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the use of AsyncTask instead of a raw thread.
				final Context context = this;
				mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// Register on my server
						// On server creates a new user
						// ServerUtilities.register(context, name, email,
						// regId);
						ServerUtilities.register(context, kullaniciID2,
								kullaniciID2, myDeviceId, regId);

						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						mRegisterTask = null;
					}

				};
				mRegisterTask.execute(null, null, null);
			}
		}

		setupContainer(share.getString("userType", "0"));
		setupDrawer();
	}

	/**
	 * Receiving push messages
	 * */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 * */

			// Showing received message
			// lblMessage.append(newMessage + "\n");
			Toast.makeText(getApplicationContext(),
					"New Message: " + newMessage, Toast.LENGTH_LONG).show();

			// Releasing wake lock
			WakeLocker.release();
		}
	};

	/**
	 * Setup the drawer layout. This method also includes the method calls for
	 * setting up the Left side drawer.
	 */
	private void setupDrawer() {
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			@Override
			public void onDrawerClosed(View view) {
				setActionBarTitle();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle("E-Kamp�s");
			}
		};
		drawerLayout.setDrawerListener(drawerToggle);
		drawerLayout.closeDrawers();

		setupLeftNavDrawer();
	}

	/**
	 * Setup the left navigation drawer/slider. You can add your logic to load
	 * the contents to be displayed on the left side drawer. You can also setup
	 * the Header and Footer contents of left drawer if you need them.
	 */
	@SuppressLint("InflateParams")
	private void setupLeftNavDrawer() {
		drawerLeft = (ListView) findViewById(R.id.left_drawer);

		View header = getLayoutInflater().inflate(R.layout.left_nav_header,
				null);
		drawerLeft.addHeaderView(header);
		TextView nameSurname = (TextView) findViewById(R.id.tv_namesurname);
		nameSurname.setText(share.getString("NameSurname",
				"Name can not take to web service."));
		TextView durum = (TextView) findViewById(R.id.tv_announcement);
		final String userType = share.getString("userType", "0");

		if (userType.equals("1")) {
			drawerLeft.setAdapter(new LeftNavAdapter(this, studentMenuList()));
			durum.setText(getResources().getString(R.string.student));
		} else if (userType.equals("2")) {
			drawerLeft.setAdapter(new LeftNavAdapter(this, teacherMenuList()));
			durum.setText(getResources().getString(R.string.teacher));
		} else if (userType.equals("3")) {
			// Admin Menu Items
		} else if (userType.equals("0")) {
			// Error Menu Items
		}

		drawerLeft.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				drawerLayout.closeDrawers();
				launchFragment(pos, userType);
			}
		});
		drawerLayout.openDrawer(drawerLeft);
	}

	/**
	 * This method returns a list of dummy items for left navigation slider. You
	 * can write or replace this method with the actual implementation for list
	 * items.
	 * 
	 * @return the dummy items
	 */
	private ArrayList<Data> studentMenuList() {
		ArrayList<Data> al = new ArrayList<Data>();
		al.add(new Data("Sohbet", null, R.drawable.ic_chat));
		al.add(new Data("Duyurular", null, R.drawable.ic_notes));
		al.add(new Data("Dosyalar", null, R.drawable.ic_projects));
		al.add(new Data("Videolar", null, R.drawable.ic_projects));
		al.add(new Data("Ayarlar", null, R.drawable.ic_setting));
		al.add(new Data("E-Kamp�s", null, R.drawable.ic_about));
		al.add(new Data("��k��", null, R.drawable.ic_logout));
		return al;
	}

	private ArrayList<Data> teacherMenuList() {
		ArrayList<Data> al = new ArrayList<Data>();
		al.add(new Data("Sohbet", null, R.drawable.ic_chat));
		al.add(new Data("Duyurular", null, R.drawable.ic_notes));
		al.add(new Data("Dosya ��lemleri", null, R.drawable.ic_projects));
		al.add(new Data("Videolar", null, R.drawable.ic_projects));
		al.add(new Data("Ayarlar", null, R.drawable.ic_setting));
		al.add(new Data("E-Kamp�s", null, R.drawable.ic_about));
		al.add(new Data("��k��", null, R.drawable.ic_logout));
		return al;
	}

	/**
	 * This method can be used to attach Fragment on activity view for a
	 * particular tab position. You can customize this method as per your need.
	 * 
	 * @param pos
	 *            the position of tab selected.
	 */
	private void launchFragment(int pos, String userType) {
		Fragment f = null;
		String title = null;

		if (pos == 1) {
			title = "Sohbet";
			f = new ChatList();
		} else if (pos == 2) {
			title = "Duyurular";
			f = new AnnouncementList();
		} else if (pos == 3) {
			title = "Dersler";
			f = new LessonList();
		} else if (pos == 4) {
			title = "Videolar";
			f = new VideoList();
		} else if (pos == 5) {
			title = "Ayarlar";
		} else if (pos == 6) {
			title = "E-Kamp�s";
			f = new AboutChat();
		} else if (pos == 7) {
			Editor ed = share.edit();
			ed.putString("kullaniciAdi", "");
			ed.putString("kullaniciID", "");
			ed.putString("sifre", "");
			ed.putString("Eposta", "");
			ed.putString("userType", "");
			ed.commit();
			finish();
		}

		if (f != null) {
			while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				getSupportFragmentManager().popBackStackImmediate();
			}
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, f).addToBackStack(title)
					.commit();
		}
	}

	/**
	 * Setup the container fragment for drawer layout. The current
	 * implementation of this method simply calls launchFragment method for tab
	 * position 1 as the position 0 is for List header view. You can customize
	 * this method as per your need to display specific content.
	 */
	private void setupContainer(String userType) {
		getSupportFragmentManager().addOnBackStackChangedListener(
				new OnBackStackChangedListener() {

					@Override
					public void onBackStackChanged() {
						setActionBarTitle();
					}
				});
		launchFragment(6, userType);
	}

	/**
	 * Set the action bar title text.
	 */
	private void setActionBarTitle() {
		if (drawerLayout.isDrawerOpen(drawerLeft)) {
			getActionBar().setTitle(R.string.app_name);
			return;
		}
		if (getSupportFragmentManager().getBackStackEntryCount() == 0)
			return;
		String title = getSupportFragmentManager().getBackStackEntryAt(
				getSupportFragmentManager().getBackStackEntryCount() - 1)
				.getName();
		getActionBar().setTitle(title);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggle
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
				getSupportFragmentManager().popBackStackImmediate();
			} else
				finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
