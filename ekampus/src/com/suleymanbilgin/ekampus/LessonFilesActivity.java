package com.suleymanbilgin.ekampus;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.helper.Constant;
import com.suleymanbilgin.ekampus.helper.Utils;
import com.suleymanbilgin.ekampus.model.LessonFile;
import com.suleymanbilgin.ekampus.services.DownloadService;

public class LessonFilesActivity extends CustomActivity implements
		OnItemClickListener {

	private static final String TAG = "LessonFilesActivity";

	RequestQueue mRequestQueue;

	/** The Activity list. */
	private ArrayList<LessonFile> lessonFileList;
	private ArrayList<LessonFile> lfList = new ArrayList<LessonFile>();
	private LessonFileAdapter lessonFileAdapter;

	SharedPreferences share;

	String userID;
	String userPassword;

	private String value = "";
	private ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lesson_files_activity);

		init();

	}

	private void init() {
		mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		lessonFileList = new ArrayList<LessonFile>(lfList);
		ListView list = (ListView) findViewById(R.id.list);

		mProgressDialog = new ProgressDialog(LessonFilesActivity.this);

		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			value = extras.getString("lessonID");
		}

		loadLessonFileList(value);

		lessonFileAdapter = new LessonFileAdapter();
		list.setAdapter(lessonFileAdapter);
		list.setOnItemClickListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

	private void loadLessonFileList(String lessonID) {
		final ProgressDialog pd = new ProgressDialog(this);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(true);
		pd.show();

		String URL = getString(R.string.service_url)
				+ getString(R.string.dosya_listesi) + "?dersID=" + lessonID;

		JsonArrayRequest json = new JsonArrayRequest(URL,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						AppLog.Log(TAG, response.toString());
						JSONObject tmp;
						try {
							for (int i = 0; i < response.length(); i++) {

								tmp = response.getJSONObject(i);
								// Variables
								String DosyaID = tmp.getString("DosyaID");
								String url = tmp.getString("url");
								String name = tmp.getString("name");
								String type = tmp.getString("type");
								String dosyaHafta = tmp.getString("dosyaBolum");
								String silinme = tmp.getString("silinme");

								if (silinme.equals("False")) {
									// Fill the list
									lfList.add(new LessonFile(DosyaID, url,
											name, type, dosyaHafta));
								} else {

								}
							}

							lessonFileList.addAll(lfList);
							lessonFileAdapter.notifyDataSetChanged();
							pd.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
							pd.dismiss();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {

						pd.dismiss();
					}
				});

		mRequestQueue.add(json);
	}

	private class LessonFileAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return lessonFileList.size();
		}

		@Override
		public LessonFile getItem(int arg0) {
			return lessonFileList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@SuppressLint("InflateParams")
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getApplicationContext()).inflate(
						R.layout.lesson_file_item, null);

			LessonFile c = getItem(pos);
			TextView tvAnnouncementTitle = (TextView) v
					.findViewById(R.id.tv_lesson_file_title);
			tvAnnouncementTitle.setText(c.getName());

			TextView tvAnnouncementCode = (TextView) v
					.findViewById(R.id.tv_lesson_file_content);
			tvAnnouncementCode.setText(c.getFileType());

			TextView tvAnnouncementWeek = (TextView) v
					.findViewById(R.id.tv_lesson_file_week);
			tvAnnouncementWeek.setText(c.getWeek());

			return v;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		AppLog.Log(TAG, String.valueOf(position));

		LessonFile tempLessonFile = (LessonFile) parent
				.getItemAtPosition(position);

		AppLog.Log(TAG, "Temp File Informations :: " + tempLessonFile.getUrl()
				+ " " + tempLessonFile.getFileType());

		String localPath = Environment.getExternalStorageDirectory()
				.getAbsoluteFile()
				+ File.separator
				+ Constant.ROOT_FOLDER_NAME
				+ File.separator
				+ Constant.FOLDER_FILES
				+ File.separator
				+ value
				+ File.separator
				+ tempLessonFile.getUrl().replaceAll(" ", "%20");

		File file = new File(localPath);
		if (file.exists()) {
			TextView tempTextview = (TextView) view
					.findViewById(R.id.tv_lesson_file_content);

			if (tempTextview.getText().equals("DOC")) {
				Utils.showToast("DOC", getApplicationContext());
				openDocument(localPath);
			} else if (tempTextview.getText().equals("PDF")) {
				Utils.showToast("PDF", getApplicationContext());
				viewPdf(Uri.fromFile(file));
			} else if (tempTextview.getText().equals("RAR")) {
				Utils.showToast("RAR", getApplicationContext());
				openRar(localPath);
			}
		} else {
			Intent intent;
			intent = new Intent(this, DownloadService.class);
			intent.putExtra("url", tempLessonFile.getUrl());
			intent.putExtra("lessonid", value);
			AppLog.Log(TAG, "FILE URL :: " + tempLessonFile.getUrl());
			intent.putExtra("receiver", new DownloadReceiver(new Handler()));
			startService(intent);

		}
	}

	private class DownloadReceiver extends ResultReceiver {
		public DownloadReceiver(Handler handler) {
			super(handler);
		}

		@Override
		protected void onReceiveResult(int resultCode, Bundle resultData) {
			super.onReceiveResult(resultCode, resultData);
			if (resultCode == DownloadService.UPDATE_PROGRESS) {
				int progress = resultData.getInt("progress");
				AppLog.Log(TAG, "Progress :: " + progress);

				mProgressDialog.setMessage("Downloading file..");
				mProgressDialog
						.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				mProgressDialog.setCancelable(false);
				mProgressDialog.show();
				mProgressDialog.setProgress(progress);

				if (progress == 100) {
					mProgressDialog.dismiss();
				}
			}
		}
	}

	private void viewPdf(Uri file) {
		Intent intent;
		intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(file, "application/pdf");
		try {
			startActivity(intent);
		} catch (ActivityNotFoundException e) {
			// No application to view, ask to download one
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("No Application Found");
			builder.setMessage("Download one from Android Market?");
			builder.setPositiveButton("Yes, Please",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent marketIntent = new Intent(Intent.ACTION_VIEW);
							marketIntent.setData(Uri
									.parse("market://details?id=com.adobe.reader"));
							startActivity(marketIntent);
						}
					});
			builder.setNegativeButton("No, Thanks", null);
			builder.create().show();
		}
	}

	public void openDocument(String localpath) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		File file = new File(localpath);
		String extension = android.webkit.MimeTypeMap
				.getFileExtensionFromUrl(Uri.fromFile(file).toString());
		String mimetype = android.webkit.MimeTypeMap.getSingleton()
				.getMimeTypeFromExtension(extension);
		if (extension.equalsIgnoreCase("") || mimetype == null) {

			// if there is no extension or there is no definite mimetype, still
			// try to open the file
			intent.setDataAndType(Uri.fromFile(file), "text/*");
		} else {
			intent.setDataAndType(Uri.fromFile(file), mimetype);
		}
		// custom message for the intent
		startActivity(Intent.createChooser(intent, "Choose an Application:"));
	}
	
	public void openRar(String localpath) {
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
		File file = new File(localpath);
		String extension = android.webkit.MimeTypeMap
				.getFileExtensionFromUrl(Uri.fromFile(file).toString());
		String mimetype = android.webkit.MimeTypeMap.getSingleton()
				.getMimeTypeFromExtension(extension);
		if (extension.equalsIgnoreCase("") || mimetype == null) {

			// if there is no extension or there is no definite mimetype, still
			// try to open the file
			intent.setDataAndType(Uri.fromFile(file), "application/x-rar-compressed");
		} else {
			intent.setDataAndType(Uri.fromFile(file), mimetype);
		}
		// custom message for the intent
		startActivity(Intent.createChooser(intent, "Choose an Application:"));
	}
}
