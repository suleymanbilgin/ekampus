package com.suleymanbilgin.ekampus.services;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.helper.Constant;
import com.suleymanbilgin.ekampus.internet.Urls;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.ResultReceiver;

public class DownloadService extends Service {

	private static final String TAG = "DownloadService";

	public static final int UPDATE_PROGRESS = 8344;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		AppLog.Log(TAG, "****Lets Start Download*****");

		if (intent == null) {
		} else {

			final String urlToDownload = intent.getStringExtra("url").replaceAll(" ", "%20");
			final String localFolderPath = intent
					.getStringExtra("localFolderPath");
			final String lessonid = intent.getStringExtra("lessonid");
			final ResultReceiver receiver = (ResultReceiver) intent
					.getParcelableExtra("receiver");

			AppLog.Log(TAG, "VARIABLES :: " + urlToDownload + " "
					+ localFolderPath + " " + lessonid + " ");
			final String URL = Urls.BASE_FILES + lessonid + File.separator
					+ urlToDownload;

			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						URL url = new URL(URL);
						AppLog.Log(TAG, "**********************url=" + url
								+ "******************");
						URLConnection connection = url.openConnection();
						connection.connect();
						// this will be useful so that you can show a typical
						// 0-100%
						// progress bar
						int fileLength = connection.getContentLength();
						AppLog.Log(TAG,
								"**********************OKAY******************");
						// download the file
						InputStream input = new BufferedInputStream(url
								.openStream());
						AppLog.Log(TAG, "will create folder :: "
								+ Environment.getExternalStorageDirectory()
										.getAbsoluteFile() + File.separator
								+ Constant.ROOT_FOLDER_NAME + File.separator
								+ Constant.FOLDER_FILES + File.separator
								+ lessonid);
						File folder = new File(Environment
								.getExternalStorageDirectory()
								.getAbsoluteFile()
								+ File.separator
								+ Constant.ROOT_FOLDER_NAME
								+ File.separator
								+ Constant.FOLDER_FILES
								+ File.separator + lessonid);
						folder.mkdirs();

						String localPath = Environment
								.getExternalStorageDirectory()
								.getAbsoluteFile()
								+ File.separator
								+ Constant.ROOT_FOLDER_NAME
								+ File.separator
								+ Constant.FOLDER_FILES
								+ File.separator + lessonid
								// + localFolderPath
								+ File.separator + urlToDownload;

						AppLog.Log(TAG, "Path :: " + localPath);

						OutputStream output = new FileOutputStream(localPath);

						byte data[] = new byte[1024];
						long total = 0;
						int count;
						while ((count = input.read(data)) != -1) {
							total += count;
							// publishing the progress....
							Bundle resultData = new Bundle();
							resultData.putInt("progress",
									(int) (total * 100 / fileLength));
							try {
								receiver.send(UPDATE_PROGRESS, resultData);
							} catch (Exception e) {
								// TODO: handle exception
							}
							output.write(data, 0, count);
						}

						output.flush();
						output.close();
						input.close();
					} catch (IOException e) {
						AppLog.Log(TAG, "********* EXCEPTION *****");
						e.printStackTrace();
					}

					Bundle resultData = new Bundle();
					resultData.putInt("progress", 100);
					try {
						receiver.send(UPDATE_PROGRESS, resultData);
					} catch (Exception e) {
						// TODO: handle exception
					}
					stopSelf();
				}
			}).start();
		}

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
