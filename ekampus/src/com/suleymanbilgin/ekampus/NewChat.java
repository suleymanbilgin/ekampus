package com.suleymanbilgin.ekampus;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.suleymanbilgin.ekampus.R;
import com.suleymanbilgin.ekampus.custom.CustomActivity;

public class NewChat extends CustomActivity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_chat);

		setTouchNClick(R.id.btnAdd);
		setTouchNClick(R.id.btnProject);
		setTouchNClick(R.id.btnSend);

		getActionBar().setLogo(R.drawable.icon_trans);
	}

	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.btnSend)
		{
			finish();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.newchat, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}
}
