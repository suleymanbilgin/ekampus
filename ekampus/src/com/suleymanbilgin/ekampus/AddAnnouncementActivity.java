package com.suleymanbilgin.ekampus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.model.Lesson;

public class AddAnnouncementActivity extends CustomActivity {
	Spinner sampleSpinner;
	String selectedItem;
	String hafta = "Genel Kaynaklar";
	Bundle extras;
	String course_id;
	RequestQueue mRequestQueue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.announcement_add_activity);

		mRequestQueue = Volley.newRequestQueue(AddAnnouncementActivity.this);

		extras = getIntent().getExtras();
		if (extras == null) {

		} else {
			course_id = extras.getString("lessonID");
		}

		sampleSpinner = (Spinner) findViewById(R.id.spin_hafta);

		String[] content = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"10" };

		ArrayAdapter adapter = new ArrayAdapter(this,
				android.R.layout.simple_spinner_item, content);

		adapter.setDropDownViewResource(R.layout.spinner_layout);

		sampleSpinner.setAdapter(adapter);

		sampleSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				selectedItem = parent.getItemAtPosition(pos).toString();
				if (selectedItem.equals("0")) {
					hafta = "Genel Kaynaklar";
				} else if (selectedItem.equals("1")) {
					hafta = "1.Hafta";
				} else if (selectedItem.equals("2")) {
					hafta = "2.Hafta";
				} else if (selectedItem.equals("3")) {
					hafta = "3.Hafta";
				} else if (selectedItem.equals("4")) {
					hafta = "4.Hafta";
				} else if (selectedItem.equals("5")) {
					hafta = "5.Hafta";
				} else if (selectedItem.equals("6")) {
					hafta = "6.Hafta";
				} else if (selectedItem.equals("7")) {
					hafta = "7.Hafta";
				} else if (selectedItem.equals("8")) {
					hafta = "8.Hafta";
				} else if (selectedItem.equals("9")) {
					hafta = "9.Hafta";
				} else if (selectedItem.equals("10")) {
					hafta = "10.Hafta";
				}
				Toast.makeText(getApplicationContext(),
						selectedItem + ".Hafta se�ildi", Toast.LENGTH_SHORT)
						.show();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

		final EditText ed_title = (EditText) findViewById(R.id.ed_title);
		final EditText ed_content = (EditText) findViewById(R.id.ed_content);

		Button btn_add = (Button) findViewById(R.id.button1);
		btn_add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				add_announcement(selectedItem, ed_title.getText().toString(),
						ed_content.getText().toString(), hafta);
			}
		});
	}

	private void add_announcement(String selectedItem, String title,
			String content, String hafta) {
		final ProgressDialog pd = new ProgressDialog(AddAnnouncementActivity.this);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(false);
		pd.show();
		
		String URL = getResources().getString(R.string.service_url)
				+ getResources().getString(R.string.duyuru_ekle) + "?title="
				+ title + "&content=" + content + "&haftaID=" + selectedItem
				+ "&hafta=" + hafta + "&courseID=" + course_id;

		StringRequest request = new StringRequest(URL,
				new Response.Listener<String>() {
					public void onResponse(String response) {
						pd.dismiss();
						finish();
					};
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						pd.dismiss();
					};
				});

		mRequestQueue.add(request);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}

}
