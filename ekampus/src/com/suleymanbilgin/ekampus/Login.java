package com.suleymanbilgin.ekampus;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.androidquery.util.AQUtility;
import com.suleymanbilgin.ekampus.custom.CustomActivity;
import com.suleymanbilgin.ekampus.helper.AppLog;
import com.suleymanbilgin.ekampus.helper.Utils;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity.
 */
public class Login extends CustomActivity {

	private static final String TAG = "Login";
	
	EditText username, password;
	RadioButton radio;

	RequestQueue mRequestQueue;

	SharedPreferences share;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		init();

		AppLog.Log(TAG, "Login Activity");

		setTouchNClick(R.id.btn_login);
		setTouchNClick(R.id.btnFb);
		setTouchNClick(R.id.btnTw);
		setTouchNClick(R.id.btn_forgot_password);
	}

	private void init() {
		mRequestQueue = Volley.newRequestQueue(getApplicationContext());

		username = (EditText) findViewById(R.id.et_user_name);
		password = (EditText) findViewById(R.id.et_password);

		share = getApplicationContext().getSharedPreferences("appdata", 0);

		File file = new File(Utils.getBasePath());
		AQUtility.setCacheDir(file);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v.getId() == R.id.btn_login) {
			login();
		}
	}

	private void login() {
		final ProgressDialog pd = new ProgressDialog(Login.this);
		pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		pd.setMessage(getString(R.string.loading));
		pd.setCancelable(false);
		pd.show();

		if (username.getText().toString().equals("")
				|| password.getText().toString().equals("")) {
			Utils.showToast(
					getResources()
							.getString(R.string.required_blank_parameters),
					Login.this);
			pd.dismiss();
			return;
		}

		String URL = getString(R.string.service_url)
				+ getString(R.string.kullanici_giris) + "?kullaniciAdi="
				+ username.getText().toString() + "&sifre="
				+ md5(password.getText().toString()) + "&requesterID="
				+ getRequesterID();

		AppLog.Log(TAG, "URL :: " + URL);

		JsonObjectRequest json = new JsonObjectRequest(URL, null,
				new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						try {
							AppLog.Log(TAG, "JSON RESPONSE :: " + response);
							if (response.getInt("Code") == 0) {
								Utils.showToast(
										getResources().getString(
												R.string.do_not_login),
										Login.this);
							} else {
								// Intent intent = new Intent(
								// getApplicationContext(),
								// MainActivity.class);

								Editor ed = share.edit();
								ed.putString("kullaniciAdi",
										response.getString("KullaniciAdi"));
								ed.putString("kullaniciID",
										response.getString("ID"));
								ed.putString("NameSurname",
										response.getString("Ad"));
								ed.putString("sifre",
										md5(response.getString("Sifre")));
								ed.putString("sifreduz", password.getText()
										.toString());
								ed.putString("Eposta",
										response.getString("Eposta"));
								ed.putString("bolumID",
										response.getString("BolumID"));
								ed.putString("userType",
										response.getString("KullaniciTip"));
								ed.commit();

								if (share.getString("kullaniciAdi", "").equals(
										"")
										&& share.getString("kullaniciID", "")
												.equals("")
										&& share.getString("sifre", "").equals(
												"")
										&& share.getString("Eposta", "")
												.equals("")) {

								} else {
									Log.e("****",
											"kullanıcıID"
													+ share.getString(
															"kullaniciID", ""));
									Intent application = new Intent(
											getApplicationContext(),
											MainActivity.class);
									application.putExtra("kullaniciID2",
											share.getString("kullaniciID", ""));
									startActivity(application);
								}
								// startActivity(intent);

							}
							pd.dismiss();
							
						} catch (Exception e) {
							pd.dismiss();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						pd.dismiss();
					}
				});
		mRequestQueue.add(json);
	}

	/**
	 * function md5 encryption for passwords
	 * 
	 * @param password
	 * @return passwordEncrypted
	 */
	private static final String md5(final String password) {
		try {

			MessageDigest digest = java.security.MessageDigest
					.getInstance("MD5");
			digest.update(password.getBytes());
			byte messageDigest[] = digest.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
}
